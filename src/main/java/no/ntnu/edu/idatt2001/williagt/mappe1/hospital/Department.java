package no.ntnu.edu.idatt2001.williagt.mappe1.hospital;

import no.ntnu.edu.idatt2001.williagt.mappe1.hospital.exception.RemoveException;

import java.util.ArrayList;
import java.util.Objects;

public class Department {
    private String departmentName;

    private ArrayList<Employee> employees = new ArrayList<>();
    private ArrayList<Patient> patients = new ArrayList<>();

    public Department(String departmentName){
        this.departmentName = departmentName;
    }

    public void setDepartmentName(String newDepartmentName){
        this.departmentName = newDepartmentName;
    }

    public String getDepartmentName(){
        return departmentName;
    }

    public void addEmployee(Employee employee){
        employees.add(employee);
    }

    public ArrayList<Employee> getEmployees(){
        return employees;
    } //This method is returning a bunch of mutable objects. This compromises their security and affects encapsulation
      //in a negative way.

    public void addPatient(Patient patient){
        patients.add(patient);
    }

    public ArrayList<Patient> getPatients(){
        return patients;
    } //This method is returning a bunch of mutable objects. This compromises their security and affects encapsulation
      //in a negative way.

    /**
     * Method for removing a person from the department. Works for both employees and patients.
     * To remove an employee, both socialSecurityNumber, firstName, lastName and class name has to match the persons.
     * The last point means that the job positions have to match too.
     *
     * To remove a patient, only socialSecurityNumber, firstName and lastName has to match the persons.
     *
     * If neither an employee nor patient is removed, a RemoveException is thrown.
     *
     * @param person - the person that is to be removed
     * @throws RemoveException - if a person is not removed
     */
    public void remove(Person person) throws RemoveException {
        boolean removedPerson = false; //Used for exception

        if(person instanceof Employee){ //For employees:
            for(Employee employee : employees){
                if(employee.getSocialSecurityNumber().equals(person.getSocialSecurityNumber()) //If the socialSecurityNumber..
                        && employee.getFirstName().equals(person.getFirstName())//.., firstName..
                        && employee.getLastName().equals(person.getLastName()) //.., lastName and..
                        && employee.getClass().getName().equals(person.getClass().getName())){ //.. the class names match:
                    removedPerson = employees.remove(employee); //remove the person and set removedPerson to true
                    break;
                }
            }
        }

        if(person instanceof Patient){ //For patients:
            for(Patient patient : patients) {
                if (patient.getSocialSecurityNumber().equals(person.getSocialSecurityNumber()) //If the socialSecurityNumber..
                        && patient.getFirstName().equals(person.getFirstName()) //.., firstName and
                        && patient.getLastName().equals(person.getLastName())) {//.. lastName match:
                    removedPerson = patients.remove(patient); //remove the person and set removedPerson to true
                    break;
                }
            }
        }

        if(!removedPerson){ //If the given person is not removed, a RemoveException will be thrown
            throw new RemoveException("The person you tried to remove did not exist in the register.");
        }
    } //A comment about this implementation: it could've been done in fewer lines of code by implementing
      //an equals method in all classes that inherit from Person. This would've made it possible to use
      //ArrayLists remove method which uses an objects equals method.

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Department)) return false;
        Department that = (Department) o;
        return Objects.equals(departmentName, that.departmentName);
    } //A comment about this implementation: it only checks if two departments have the same name.
      //If there had been two departments in a large Hospital with the same name, these two would've been equal according
      //to this method, even if they had contained different personal or patients.
      //If all the classes that inherit from Person had equals methods, this method could've used them too, and the
      //potential problem of two departments with same name and different personal/patients would not exist.
      //This does not mean it is impossible to check for equality of every person in the department, it would just
      //not be a pretty implementation and it would not be very cohesive.

    @Override
    public int hashCode() {
        return Objects.hash(departmentName);
    }

    @Override
    public String toString(){
        String employees = "";
        for(Employee employee : this.employees){
            employees += employee.toString() + "\n";
        }

        String patients = "";
        for(Patient patient : this.patients){
            patients += patient.toString() + "\n";
        }

        return departmentName + ":\n" +
                "Employees: \n" + employees +
                "Patients: \n" + patients;
    }
}
