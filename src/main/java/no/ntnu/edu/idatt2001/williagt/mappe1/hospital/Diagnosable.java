package no.ntnu.edu.idatt2001.williagt.mappe1.hospital;

public interface Diagnosable {

    void setDiagnosis(String diagnosis);
}
