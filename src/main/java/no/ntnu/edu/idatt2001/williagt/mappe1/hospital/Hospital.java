package no.ntnu.edu.idatt2001.williagt.mappe1.hospital;

import java.util.ArrayList;

public class Hospital {
    private final String hospitalName;

    private ArrayList<Department> hospital = new ArrayList<>();

    public Hospital(String hospitalName){
        this.hospitalName = hospitalName;
    }

    public String getHospitalName(){
        return hospitalName;
    }

    public void addDepartment(Department department){
        hospital.add(department);
    }

    public ArrayList<Department> getDepartments(){
        return hospital;
    }

    @Override
    public String toString(){

        String departments = "";
        for (Department department : hospital){
            departments += department.toString() + "\n";
        }
        return "~~~" + hospitalName + "~~~\n\n" + departments;
    }

}
