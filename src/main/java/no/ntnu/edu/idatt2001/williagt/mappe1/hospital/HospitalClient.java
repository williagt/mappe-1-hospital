package no.ntnu.edu.idatt2001.williagt.mappe1.hospital;

import no.ntnu.edu.idatt2001.williagt.mappe1.hospital.exception.RemoveException;

public class HospitalClient {
    public static void main(String[] args) {
        Hospital rikshospitalet = new Hospital("Riskhospitalet"); //Instantiating a Hospital
        HospitalTestData.fillRegisterWithTestData(rikshospitalet); //Filling the Hospital with test data

        //Removing an employee:
        System.out.println("~~~~Initial register~~~~\n");
        System.out.println(rikshospitalet.toString());
        try{
            Employee firstEmployee = rikshospitalet.getDepartments().get(0).getEmployees().get(0);
            rikshospitalet.getDepartments().get(0).remove(firstEmployee);
        }catch(RemoveException e){
            System.err.println(e);
        }
        System.out.println("\n~~~~Register after removing the first employee~~~~");
        System.out.println(rikshospitalet.toString());

        //Removing a non-existent patient:
        System.out.println("Here a non-existent patient is attempted removed, and thus an exception is thrown: ");
        try{
            Patient doesntExist = new Patient("Doesn't", "Exist", "");
            rikshospitalet.getDepartments().get(1).remove(doesntExist);
        }catch (RemoveException e){
            System.err.println(e);
        }
    }
}
