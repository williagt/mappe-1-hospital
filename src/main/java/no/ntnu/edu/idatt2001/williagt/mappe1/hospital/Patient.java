package no.ntnu.edu.idatt2001.williagt.mappe1.hospital;

public class Patient extends Person implements Diagnosable {
    private String diagnosis = "";

    protected Patient(String firstName, String lastName, String socialSecurityNumber){
        super(firstName, lastName, socialSecurityNumber);
    }

    protected String getDiagnosis(){
        return diagnosis;
    }

    @Override
    public void setDiagnosis(String diagnosis){
        this.diagnosis = diagnosis;
    }

    /**
     * Returns the first and last name of a patient and their social security number. If the patient has a
     * diagnosis this is also returned. Else this is entirely omitted.
     *
     * @return A String containing essential information about a patient.
     */
    @Override
    public String toString(){
        if((!diagnosis.isEmpty())){
            return super.toString() + ". Diagnosis: " + diagnosis;
        }
        return super.toString();
    }
}
