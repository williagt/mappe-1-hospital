package no.ntnu.edu.idatt2001.williagt.mappe1.hospital;

public abstract class Person {
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    public Person(String firstName, String lastName, String socialSecurityNumber){
        //Here would be an excellent place to check for whether input is right when instantiating a person.
        //E.g. this constructor could throw an IllegalArgumentException if either of the parameters are empty.
        //But since all the persons in the test data have empty strings as social security number, and I don't want to change anything
        //in the task we were given, this check has been left out.
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getSocialSecurityNumber(){
        return socialSecurityNumber;
    }

    public String getFirstName(){
        return firstName;
    }

    public String getLastName(){
        return lastName;
    }

    public String getFullName(){return firstName + " " + lastName;}

    public void setSocialSecurityNumber(String newSocialSecurityNumber){
        this.socialSecurityNumber = newSocialSecurityNumber;
    }

    public void setFirstName(String newFirstName){
        this.firstName = newFirstName;
    }

    public void setLastName(String newLastName){
        this.lastName = newLastName;
    }

    @Override
    public String toString(){
        return firstName + " " + lastName + " - " + socialSecurityNumber;
    }
}
