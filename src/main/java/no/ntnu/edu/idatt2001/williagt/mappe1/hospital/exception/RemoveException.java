package no.ntnu.edu.idatt2001.williagt.mappe1.hospital.exception;

public class RemoveException extends Exception{
    private static final long serialVersionUID = 1L;

    public RemoveException(String e){
        super(e);
    }
}
