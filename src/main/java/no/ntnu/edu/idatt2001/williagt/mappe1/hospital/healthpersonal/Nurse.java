package no.ntnu.edu.idatt2001.williagt.mappe1.hospital.healthpersonal;

import no.ntnu.edu.idatt2001.williagt.mappe1.hospital.Employee;

public class Nurse extends Employee {

    public Nurse(String firstName, String lastName, String socialSecurityNumber){
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString(){
        return super.toString();
    } //Since neither classes that inherit from Doctor override the toString method, this class does not override the
      //toString method in any meaningful way. It would not make sense to only modify this e.g. by adding "Nurse"
      //when none of the other employees have this kind of modification.
}
