package no.ntnu.edu.idatt2001.williagt.mappe1.hospital.healthpersonal.doctor;

import no.ntnu.edu.idatt2001.williagt.mappe1.hospital.Employee;
import no.ntnu.edu.idatt2001.williagt.mappe1.hospital.Patient;

public abstract class Doctor extends Employee {

    protected Doctor(String firstName, String lastName, String socialSecurityNumber){
        super(firstName, lastName, socialSecurityNumber);
    }

    public abstract void setDiagnosis(Patient patient, String diagnosis);
}
