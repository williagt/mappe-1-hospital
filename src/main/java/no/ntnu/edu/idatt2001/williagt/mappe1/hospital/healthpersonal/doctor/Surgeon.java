package no.ntnu.edu.idatt2001.williagt.mappe1.hospital.healthpersonal.doctor;

import no.ntnu.edu.idatt2001.williagt.mappe1.hospital.Patient;

public class Surgeon extends Doctor {

    public Surgeon(String firstName, String lastName, String socialSecurityNumber){
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public void setDiagnosis(Patient patient, String diagnosis){
        patient.setDiagnosis(diagnosis);
    }
}
