package no.ntnu.edu.idatt2001.williagt.mappe1.hospital;


import no.ntnu.edu.idatt2001.williagt.mappe1.hospital.exception.RemoveException;
import no.ntnu.edu.idatt2001.williagt.mappe1.hospital.healthpersonal.Nurse;
import no.ntnu.edu.idatt2001.williagt.mappe1.hospital.healthpersonal.doctor.GeneralPractitioner;
import no.ntnu.edu.idatt2001.williagt.mappe1.hospital.healthpersonal.doctor.Surgeon;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class DepartmentTest {

    @Nested
    @DisplayName("Remove method tests")
    class RemoveMethodTests{

        Department radiology = new Department("Radiology");

        Nurse nurse1 = new Nurse("Iren", "Johansen", "1");
        GeneralPractitioner generalPractitioner1 = new GeneralPractitioner("Arne", "Henriksen", "2");
        Surgeon surgeon1 = new Surgeon("Ann", "Christiansen", "3");
        Patient patient1 = new Patient("Knut", "Larsen", "4");

        @BeforeEach
        void addPersonsToDepartment(){
            radiology.addEmployee(nurse1);
            radiology.addEmployee(generalPractitioner1);
            radiology.addEmployee(surgeon1);
            radiology.addPatient(patient1);
        }

        @Nested
        @DisplayName("Remove a person that is registered in a department")
        class RemoveAPersonRegisteredInADepartment{

            @Test
            @DisplayName("Remove a nurse")
            void removeANurse(){
                try{
                    radiology.remove(nurse1);

                }catch (RemoveException e){
                    System.err.println(e);
                }

                assertFalse(radiology.getEmployees().contains(nurse1));
            }

            @Test
            @DisplayName("Remove a general practitioner")
            void removeAGeneralPractitioner(){
                try{
                    radiology.remove(generalPractitioner1);

                }catch (RemoveException e){
                    System.err.println(e);
                }

                assertFalse(radiology.getEmployees().contains(generalPractitioner1));
            }

            @Test
            @DisplayName("Remove a surgeon")
            void removeASurgeon(){
                try{
                    radiology.remove(surgeon1);

                }catch (RemoveException e){
                    System.err.println(e);
                }

                assertFalse(radiology.getEmployees().contains(surgeon1));
            }

            @Test
            @DisplayName("Remove a patient")
            void removeAPatient(){
                try{
                    radiology.remove(patient1);

                }catch (RemoveException e){
                    System.err.println(e);
                }

                assertFalse(radiology.getEmployees().contains(patient1));
            }
        }

        @Nested
        @DisplayName("Remove a person that is not registered in a department from that department")
        class RemoveAPersonThatIsNotRegisteredInADepartmentFromThatDepartment{

            @Test
            @DisplayName("Remove a nurse that is not registered")
            void removeANurseThatIsNotRegistered(){
                Nurse nurse2 = new Nurse("Ola", "Flo", "5");

                try{
                    radiology.remove(nurse2);
                }catch(RemoveException e){
                    assertEquals(e.getMessage(), "The person you tried to remove did not exist in the register.");
                }
            }

            @Test
            @DisplayName("Remove a general practitioner that is not registered")
            void removeAGeneralPractitionerThatIsNotRegistered(){
                GeneralPractitioner generalPractitioner2 = new GeneralPractitioner("Stian", "Balasubramaniam", "6");

                try{
                    radiology.remove(generalPractitioner2);
                }catch(RemoveException e){
                    assertEquals(e.getMessage(), "The person you tried to remove did not exist in the register.");
                }
            }

            @Test
            @DisplayName("Remove a surgeon that is not registered")
            void removeASurgeonThatIsNotRegistered(){
                Surgeon surgeon2 = new Surgeon("Lena", "Nilson", "7");

                try{
                    radiology.remove(surgeon2);
                }catch(RemoveException e){
                    assertEquals(e.getMessage(), "The person you tried to remove did not exist in the register.");
                }
            }

            @Test
            @DisplayName("Remove a patient that is not registered")
            void removeAPatientThatIsNotRegistered(){
                Patient patient2 = new Patient("Kari", "Andersen", "8");

                try{
                    radiology.remove(patient2);
                }catch(RemoveException e){
                    assertEquals(e.getMessage(), "The person you tried to remove did not exist in the register.");
                }
            }
        }

        @Nested
        @DisplayName("Writing invalid input")
        class WritingInvalidInput{

            @Test
            void nullAsInput(){
                try{
                    radiology.remove(null);
                }catch(RemoveException e){
                    assertEquals(e.getMessage(), "The person you tried to remove did not exist in the register.");
                }catch (NullPointerException e){
                    assertEquals(e.getMessage(), null);
                }
            }
        }
    }
}